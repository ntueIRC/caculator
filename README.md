# Calculator
## 簡介
此專案提供一個標準的圖形化介面計算機
## 結構
前後端分離，並交由核心區塊引用各程式碼區塊。  
![structure](/uploads/adbc4bd2d5f80aa76b3c839f12fa7ec4/structure.png)